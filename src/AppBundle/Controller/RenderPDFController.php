<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use AppBundle\Entity\ExportAttempt;

class RenderPDFController extends Controller
{
    private function escapeLatexSpecialChars( $string )
    {
        $map = array(
                "#"=>"\\#",
                "$"=>"\\$",
                "%"=>"\\%",
                "&"=>"\\&",
                "~"=>"\\~{}",
                "_"=>"\\_",
                "^"=>"\\^{}",
                "\\"=>"\\textbackslash{}",
                "{"=>"\\{",
                "}"=>"\\}",
        );
        return str_replace(array_keys($map), array_values($map), $string);
        // return preg_replace( "/([\^\%~\\\\#\$%&_\{\}])/e", "\$map['$1']", $string );
    }
    /**
     * @Route("/render/{apiKey}", name="render-file")
     * @Method({"POST"})
     */
    public function indexAction(Request $request, $apiKey)
    {
        $exportDefinition = $this->getDoctrine()->getRepository("AppBundle:Export")->findOneBy(array(
            'apiKey' => $apiKey
        ));

        if($exportDefinition==null) {
            throw $this->createNotFoundException("not found");
        }

        if(!$exportDefinition->getIsEnabled())
        {
            throw new AccessDeniedHttpException();
        }

        $parameters = json_decode($request->get('parameters'), true);
        if($parameters===null) {
            throw new BadRequestHttpException('invalid parameters');
        }

        $parameters = array_replace_recursive($exportDefinition->getParameters(), $parameters);

        array_walk_recursive($parameters, function(&$p) {
            $p = preg_replace_callback( "/([\^\%~\\\\#\$%&_\{\}])/", function($m) {
                $map = array(
                        "#"=>"\\#",
                        "$"=>"\\$",
                        "%"=>"\\%",
                        "&"=>"\\&",
                        "~"=>"\\~{}",
                        "_"=>"\\_",
                        "^"=>"\\^{}",
                        "\\"=>"\\textbackslash{}",
                        "{"=>"\\{",
                        "}"=>"\\}",
                );
                return $map[$m[1]];
            }, $p);
        });

        $latexTemplate = $this->get('twig')->createTemplate($exportDefinition->getTemplate());
        $latexString = $latexTemplate->render($parameters);

        $workDir = realpath($this->get('kernel')->getRootDir().'/../var/');
        $workDir .= '/latex-to-pdf/';
        if(!file_exists($workDir))
        {
            mkdir($workDir);
        }
        $workDir .= $exportDefinition->getId().'/';
        if(!file_exists($workDir))
        {
            mkdir($workDir);
        }

        $files = $exportDefinition->getFiles();
        foreach($files as $f)
        {
            if(!file_exists($workDir.$f->getFileName()))
            {
                file_put_contents($workDir.$f->getFileName(), base64_decode($f->getFileContent()));
            }
        }

        $latexFileName = sha1(uniqid() . microtime());

        $attempt = new ExportAttempt();
        $attempt->setFileName($latexFileName);
        $attempt->setIpAddress($request->getClientIp());
        $attempt->setExport($exportDefinition);

        try {
            file_put_contents($workDir.$latexFileName, $latexString);

            $process = new Process('pdflatex ' . $latexFileName);
            $process->setWorkingDirectory($workDir);
            $process->run();

            if($process->isSuccessful())
            {
                $response = new BinaryFileResponse($workDir.$latexFileName.'.pdf');
            }
            else
            {
                $attempt->setError($process->getOutput());

                $this->get('logger')->error('can not process file', array('processOutput' => $process->getOutput() ));

                $response =  new Response('error rendering pdf file', 500);
            }
        }
        catch(\Exception $e)
        {
            $attempt->setError($e->getMessage());
        }

        $this->getDoctrine()->getManager()->persist($attempt);
        $this->getDoctrine()->getManager()->flush($attempt);

        return $response;
    }

    /**
     * @Route("/render-form/{apiKey}", name="render-form")
     * @Method({"GET"})
     */
    public function formAction(Request $request, $apiKey)
    {
        $exportDefinition = $this->getDoctrine()->getRepository("AppBundle:Export")->findOneBy(array(
            'apiKey' => $apiKey
        ));

        if($exportDefinition==null)
        {
            throw $this->createNotFoundException("not found");
        }

        return $this->render("AppBundle:RenderPDF:form.html.twig", compact('exportDefinition'));
    }
}
