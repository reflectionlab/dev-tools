<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AppCleanupRendersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:cleanup:renders')
            ->setDescription('Cleanup all old renders')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $delim = new \DateTime("now");
        $delim->sub( new \DateInterval("P1D") ); // PT300S

        $attemptsQuery = $em->createQuery('SELECT a, e FROM AppBundle:ExportAttempt a JOIN a.export e WHERE a.created < :delim');
        $attemptsQuery->setParameter('delim', $delim);

        $attemptsToRemove = $attemptsQuery->getResult();

        $workDir = realpath($this->getContainer()->get('kernel')->getRootDir().'/../var/latex-to-pdf/');

        foreach($attemptsToRemove as $a)
        {
            $fileGlob = $workDir . '/' . $a->getExport()->getId() . '/' . $a->getFileName();
            if(file_exists($fileGlob))
            {
                array_map('unlink', glob($fileGlob . '*'));
            }

            $em->remove($a);
            $em->flush();
        }
    }

}
