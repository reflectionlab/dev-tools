<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExportAttempt
 *
 * @ORM\Table(name="export_attempt")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExportAttemptRepository")
 */
class ExportAttempt
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="file_name", type="string", length=255)
     */
    private $fileName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(name="error", type="text", nullable=true)
     */
    private $error;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_address", type="string", length=16)
     */
    private $ipAddress;

    /**
     * @ORM\ManyToOne(targetEntity="Export", inversedBy="attempts"))
     * @ORM\JoinColumn(name="export_id", referencedColumnName="id")
     */
    private $export;


    public function __construct()
    {
        $this->setCreated(new \DateTime("now"));
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     *
     * @return ExportAttempt
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return ExportAttempt
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set error
     *
     * @param string $error
     *
     * @return ExportAttempt
     */
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

    /**
     * Get error
     *
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Set ipAddress
     *
     * @param string $ipAddress
     *
     * @return ExportAttempt
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set export
     *
     * @param string $export
     *
     * @return ExportAttempt
     */
    public function setExport($export)
    {
        $this->export = $export;

        return $this;
    }

    /**
     * Get export
     *
     * @return string
     */
    public function getExport()
    {
        return $this->export;
    }
}

