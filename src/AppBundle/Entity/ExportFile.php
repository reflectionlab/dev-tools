<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExpoprtFile
 *
 * @ORM\Table(name="expoprt_file")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExpoprtFileRepository")
 */
class ExportFile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="file_name", type="string", length=255)
     */
    private $fileName;

    /**
     * @var string
     *
     * @ORM\Column(name="file_content", type="text")
     */
    private $fileContent;


    /**
     * @ORM\ManyToOne(targetEntity="Export", inversedBy="files"))
     * @ORM\JoinColumn(name="export_id", referencedColumnName="id")
     */
    private $export;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     *
     * @return ExpoprtFile
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set fileContent
     *
     * @param string $fileContent
     *
     * @return ExpoprtFile
     */
    public function setFileContent($fileContent)
    {
        $this->fileContent = $fileContent;

        return $this;
    }

    /**
     * Get fileContent
     *
     * @return string
     */
    public function getFileContent()
    {
        return $this->fileContent;
    }


    /**
     * Set export
     *
     * @param string $export
     *
     * @return ExportAttempt
     */
    public function setExport($export)
    {
        $this->export = $export;

        return $this;
    }

    /**
     * Get export
     *
     * @return string
     */
    public function getExport()
    {
        return $this->export;
    }
}

